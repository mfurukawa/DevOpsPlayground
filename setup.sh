#!/bin/bash
export INVENTORY=./inventory
export ANSIBLE_CONFIG=./ansible.cfg

ansible-playbook -i ${INVENTORY} playbooks/pre-setup.yml
ansible-playbook -i ${INVENTORY} playbooks/docker-setup.yml

ansible-playbook -i ${INVENTORY} playbooks/test-docker.yml

ansible-playbook -i ${INVENTORY} playbooks/docker-network-setup.yml
ansible-playbook -i ${INVENTORY} playbooks/minikube-setup.yml

ansible-playbook -i ${INVENTORY} playbooks/test-minikube.yml

ansible-playbook -i ${INVENTORY} playbooks/gitlab-setup.yml
ansible-playbook -i ${INVENTORY} playbooks/jenkins-setup.yml
ansible-playbook -i ${INVENTORY} playbooks/jfrog-setup.yml

ansible-playbook -i ${INVENTORY} playbooks/post-setup.yml

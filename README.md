### DevOpsPlayground
- This ansible playbook make DevOps Playground environment.

### Requirement
- target OS(main target is CentOS7)
  - sample usecase of guest spec
    - virtual h/w spec: support Intel VT-x,processor(2cores), memory(7168MB) -> in case: set mem 6144MB, minikube fail to boot by least memory error...
    - network(fusion 8.5pro): custom(NAT enabled,host connect enabled,dhcp disabled)
    - specify static IP address
    - root ssh enabled
    - install CentOS7 from CentOS-7-x86_64-DVD-1611.iso with Desktop packages
- ensure setting ssh with keyfile
- ensure target can access internet
- ensuer proxy or firewall not exsist

### How to build
1. become executable to setup.sh
2. overwrite inventory/hosts to your target host
3. execute setup.sh

### Containers
1. gitlab-ce
  - public image from https://hub.docker.com/r/gitlab/gitlab-ce/
  - expose port map can specify by inventory/group_vars/all.yml
  - data store docker host directory path specfiy by gitlab_dir in inentory/group_vars/all.yml
  - name resolv in hosts file. you can access by  gitlab
2. jenkins
  - include ansible and kubectl
  - image made by dockerfile in files/JenkinsDockerFile
  - expose port map can specify by inventory/group_vars/all.yml
  - data store docker host directory path specfiy by jenkins_dir in inentory/group_vars/all.yml
  - name resolv in hosts file. you can access by  jenkins
3. jfrog
  - public image from docker.bintray.io/jfrog/artifactory-oss
  - expose port map can specify by inventory/group_vars/all.yml
  - data store docker host directory path specfiy by jfrog_dir in inentory/group_vars/all.yml
  - name resolv in hosts file. you can access by  jfrog

### Respect
- http://qiita.com/takahigashi/items/50482d492e9769548caa
- http://qiita.com/takahigashi/items/b914430ced520ecbeb11
- http://qiita.com/takahigashi/items/f979d60b1c49db814f82
